FROM python:3.6
MAINTAINER will@willthompson.co.uk
RUN useradd -d /srv --uid 61978 --no-user-group -M tarragon
ENV PYTHONUNBUFFERED 1
ADD . /srv

WORKDIR /srv
RUN pip install pipenv
RUN pipenv install --system --deploy

WORKDIR /srv/tarragon
RUN env DJANGO_SETTINGS_MODULE=tarragon.settings.dev ./manage.py collectstatic --noinput

ENV DJANGO_SETTINGS_MODULE=tarragon.settings.prod
USER tarragon
ENTRYPOINT ["/srv/launchotron-3000.sh"]
