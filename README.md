# Tarragon

Mechanically-reclaimed [countdown URLs](http://here.yt/) for a mechanically-reclaimed world.

Why “tarragon”? [No](https://en.wikipedia.org/wiki/Estragon) [reason](http://is.godot.here.yt/).

[![build status](https://gitlab.com/wjt/tarragon/badges/master/build.svg)](https://gitlab.com/wjt/tarragon/commits/master)
