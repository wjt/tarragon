#!/bin/bash
set -exu
cd /srv/tarragon
./manage.py migrate
exec gunicorn tarragon.wsgi:application \
    --name tarragon \
    --bind 0.0.0.0:8000 \
    --workers 3 \
    --log-level info \
    --access-logfile - \
    --error-logfile - \
    "$@"
