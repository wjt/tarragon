#!/usr/bin/env python3

from distutils.core import setup

setup(name='Tarragon',
      version='1.0',
      description='Bespoke Countdown Websites',
      author='Will Thompson',
      author_email='will@willthompson.co.uk',
      url='http://here.yt/',
      requires=[
          'Django',
          'Pillow',
          'whitenoise',
#          'django-hosts',
      ],
     )
