from django.contrib import admin
from .models import Countdown, Baby


class BabyInline(admin.StackedInline):
    '''
    Stacked Inline View for Baby
    '''
    model = Baby
    extra = 1


class CountdownAdmin(admin.ModelAdmin):
    '''
    Admin View for Countdown
    '''
    inlines = [
        BabyInline,
    ]


admin.site.register(Countdown, CountdownAdmin)
