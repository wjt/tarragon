import math

import django.contrib.sites.models
from django.core.validators import RegexValidator
from django.utils.functional import cached_property
from django.utils.timezone import now as tz_now
from django.db import models


def upload_to(instance, filename):
    return '{0}/{1}'.format(instance.site.domain, filename)


def baby_upload_to(instance, filename):
    return upload_to(instance.countdown, filename)


class Baby(models.Model):
    '''A baby, typically born at the end of a countdown.'''
    countdown = models.ForeignKey('Countdown', related_name='babies',
                                  on_delete=models.CASCADE)

    name = models.CharField(max_length=200)
    born_at = models.DateTimeField(blank=True, null=True)
    weight = models.CharField(max_length=50, blank=True)
    image = models.ImageField(upload_to=baby_upload_to, blank=True)
    notes = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return '{} ({})'.format(self.name, self.countdown)


# https://material.io/guidelines/style/color.html#color-color-palette
INDIGO_500 = "#3f51b5"


class Countdown(models.Model):
    '''A countdown site.'''
    site = models.OneToOneField(django.contrib.sites.models.Site,
                                related_name='+',
                                on_delete=models.CASCADE)

    title = models.CharField(max_length=200)
    deadline = models.DateTimeField()

    not_yet_text = models.CharField(max_length=100)
    not_yet_image = models.ImageField(upload_to=upload_to, blank=True)

    yes_text = models.CharField(max_length=100)
    yes_image = models.ImageField(upload_to=upload_to, blank=True)

    header_colour = models.CharField(
        max_length=20, default=INDIGO_500,
        validators=[
            RegexValidator(
                r'^#[0123456789abcdef]',
                message="Enter a colour like " + INDIGO_500,
            ),
        ])
    public = models.BooleanField(default=False)

    def __str__(self):
        return '{} ({})'.format(self.title, self.site.domain)

    @cached_property
    def seconds_remaining(self):
        '''Integer seconds until the deadline. If 0 or negative, the
        deadline has been reached.'''
        return math.ceil((self.deadline - tz_now()).total_seconds())

    @cached_property
    def has_expired(self):
        '''True if the deadline has been reached.'''
        return self.seconds_remaining <= 0

    @property
    def best_image(self):
        b = self.babies.filter(image__isnull=False).order_by('id').first()

        if b is not None:
            return b.image

        if self.has_expired:
            return self.yes_image

        return self.not_yet_image

    def get_absolute_url(self):
        '''Returns URL for the countdown site.'''
        return 'http://{}/'.format(self.site.domain)
