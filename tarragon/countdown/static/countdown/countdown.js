(function() {
    function singular(n, key) {
        if (n === 1) {
            return key.slice(0, key.length - 1);
        }

        return key;
    }
    var countdowns = document.querySelectorAll('.countdown');

    Array.prototype.forEach.call(countdowns, function(countdown) {
        var template = countdown.querySelector('.countdown-box');
        countdown.removeChild(template);

        var components = [
            {elements: {}, key: 'years'},
            {elements: {}, key: 'months'},
            {elements: {}, key: 'days'},
            {elements: {}, key: 'hours'},
            {elements: {}, key: 'minutes'},
            {elements: {}, key: 'seconds'},
        ];

        var secondsRemaining = Number.parseInt(countdown.dataset.secondsRemaining);
        var deadline = moment().add(secondsRemaining, 'seconds');

        update()
        var interval = window.setInterval(update, 1000);

        function update() {
            var diffms = deadline.diff();
            var duration = moment.duration(diffms);

            var displayed = false;
            components.forEach(function(component) {
                var elements = component.elements;
                var value = duration.get(component.key);

                if (value > 0 || displayed) {
                    displayed = true;
                    if (elements.box === undefined) {
                        var box = elements.box = template.cloneNode(true);
                        elements.value = box.querySelector('.countdown-box-value');
                        elements.label = box.querySelector('.countdown-box-label');
                        countdown.appendChild(box);
                    }

                    // TODO: can't use moment's humanize() OOTB because:
                    // a) it uses "a day", "a few minutes" etc
                    // b) no way to get the digit and string separately
                    elements.label.innerHTML = singular(value, component.key);
                    elements.value.innerHTML = value.toFixed(0);
                }

                if (value <= 0 && !displayed && elements.box !== undefined) {
                    countdown.removeChild(elements.box);
                    component.elements = {};
                }
            });

            if (duration.asSeconds() <= 0) {
                location.reload();
                window.clearInterval(interval);
                return;
            }
        }
    });
}())
