
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site
from django.http import Http404
from django.shortcuts import render

from .models import Countdown


def index(request):
    """Despatch to countdown site, main index or 404, depending on host in
    request."""

    try:
        site = get_current_site(request)
    except Site.DoesNotExist:
        raise Http404

    try:
        countdown = Countdown.objects.get(site=site)
    except Countdown.DoesNotExist:
        return real_index(request)
    else:
        return countdown_index(request, countdown)


def real_index(request):
    """Index for main domain."""
    if request.user.is_staff:
        countdowns = Countdown.objects.all()
    else:
        countdowns = Countdown.objects.filter(public=True)

    return render(request, 'countdown/index.html', {
        'countdowns': countdowns,
    })


def countdown_index(request, countdown):
    """Index for countdown!"""
    return render(request, 'countdown/countdown.html', {
        'countdown': countdown,
        'header_colour': countdown.header_colour,
        'babies': countdown.babies.order_by('id'),
    })
