from .defaults import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'gm**dcwvoei05v-$53gc)z-hkuhr@7q8#a)4512*8e9yg)!0ik'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = [
    '.hereyt.test',
]

MEDIA_URL = '/media/'
