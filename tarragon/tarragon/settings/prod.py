import os

from .defaults import *

SECRET_KEY = os.environ.get('TARRAGON_SECRET_KEY')

ALLOWED_HOSTS = [
    '.here.yt',
    '.married.yt',
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
    },
}

MEDIA_URL = 'https://media.here.yt/'

SESSION_COOKIE_SECURE = True
